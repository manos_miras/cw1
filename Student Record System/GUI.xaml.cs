﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/*  
    The purpose of the GUI Window is to easily create, edit and display a Student or a ResearchStudent instance.
    Made by Emmanuel Miras (40168970) as part of the SD2 course.
    Date last modified: 22/10/2015
*/

namespace Student_Record_System
{

    public partial class GUI : Window
    {
        public GUI()
        {
            InitializeComponent();
            cboStudentType.SelectedValue = cbi1; // Initiates combobox value to student.
        }

        bool validated = false; // Used to verify if validation has occured
        
        Student student = new Student(); // Creates a new Student called student.

        ResearchStudent researchStudent = new ResearchStudent(); // Creates a new ResearchStudent called researchStudent

        //Clears all the values from the textboxes.
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtBoxFirstName.Clear();
            txtBoxSecondName.Clear();
            txtBoxDateOfBirth.Clear();
            txtBoxCourse.Clear();
            txtBoxMatricNumber.Clear();
            txtBoxLevel.Clear();
            txtBoxCredits.Clear();

            // If researchStudent is selected
            if (cboStudentType.SelectedValue == cbi2)
            {
                txtBoxCourse.Text = "PhD";
                // Clear Topic textbox
                txtBoxTopic.Clear();
                // Clear Supervisor textbox
                txtBoxSupervisor.Clear();
            }
        }

        // Sets values entered in textboxes to relevant student properties.
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            if (cboStudentType.SelectedValue == cbi1)
            {
                // Call Student constructor into student object
                student = new Student(txtBoxFirstName.Text, txtBoxSecondName.Text, txtBoxDateOfBirth.Text, txtBoxCourse.Text,
                  txtBoxMatricNumber.Text, txtBoxLevel.Text, txtBoxCredits.Text, ref validated);
            }
            else if (cboStudentType.SelectedValue == cbi2)
            {
                // Call ResearchStudent constructor into ResearchStudent object
                researchStudent = new ResearchStudent(txtBoxFirstName.Text, txtBoxSecondName.Text, txtBoxDateOfBirth.Text,
                txtBoxMatricNumber.Text, txtBoxLevel.Text, txtBoxCredits.Text, txtBoxTopic.Text, txtBoxSupervisor.Text, ref validated);
            }
        }

        // Retrieves all of the student or researchStudent information to the textboxes.
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            if (cboStudentType.SelectedValue == cbi1)
            {
                txtBoxFirstName.Text = student.FirstName;
                txtBoxSecondName.Text = student.SecondName;
                txtBoxDateOfBirth.Text = student.DateOfBirth.ToString("dd/MM/yyyy"); // Retrieves the date in dd/MM/yyyy format.
                txtBoxCourse.Text = student.Course;
                txtBoxMatricNumber.Text = student.MatriculationNumber.ToString();
                txtBoxLevel.Text = student.Level.ToString();
                txtBoxCredits.Text = student.Credits.ToString();
            }
            else
            {
                txtBoxFirstName.Text = researchStudent.FirstName;
                txtBoxSecondName.Text = researchStudent.SecondName;
                txtBoxDateOfBirth.Text = researchStudent.DateOfBirth.ToString("dd/MM/yyyy"); // Retrieves the date in dd/MM/yyyy format.
                txtBoxCourse.Text = researchStudent.Course;
                txtBoxMatricNumber.Text = researchStudent.MatriculationNumber.ToString();
                txtBoxLevel.Text = researchStudent.Level.ToString();
                txtBoxCredits.Text = researchStudent.Credits.ToString();
                // Retrieve research student topic
                txtBoxTopic.Text = researchStudent.Topic;
                // Retrieve research student supervisor
                txtBoxSupervisor.Text = researchStudent.Supervisor;
            }
        }

        // Advances the student's or research student's level by calling advance()
        private void btnAdvance_Click(object sender, RoutedEventArgs e)
        {
            // If student is selected
            if (cboStudentType.SelectedValue == cbi1)
            {
                student.advance();
                txtBoxLevel.Text = student.Level.ToString();
            }
            else
            {
                researchStudent.advance();
                txtBoxLevel.Text = researchStudent.Level.ToString();
            }
        }

        // Clears the prompt text from the DateOfBirth textbox once user has clicked on it for the first time
        public void txtBoxDateOfBirth_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }

        // Clears the prompt text from the FirstName textbox once user has clicked on it for the first time
        public void txtBoxFirstName_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }

        // Clears the prompt text from the SecondName textbox once user has clicked on it for the first time
        public void txtBoxSecondName_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }

        // Clears the prompt text from the Course textbox once user has clicked on it for the first time
        public void txtBoxCourse_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }

        // Clears the prompt text from the Matriculation Number textbox once user has clicked on it for the first time
        public void txtBoxMatricNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }

        // Clears the prompt text from the Level textbox once user has clicked on it for the first time
        public void txtBoxLevel_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }

        // Clears the prompt text from the Credits textbox once user has clicked on it for the first time
        public void txtBoxCredits_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }

        // Clears the prompt text from the Topic textbox once user has clicked on it for the first time
        public void txtBoxTopic_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }

        // Clears the prompt text from the Supervisor textbox once user has clicked on it for the first time
        public void txtBoxSupervisor_GotFocus(object sender, RoutedEventArgs e)
        {
            PromptClear(sender);
        }
        // Award button
        private void btnAward_Click(object sender, RoutedEventArgs e)
        {
            if (!validated)
                MessageBox.Show("You have left one or more of the fields blank, please check that you have filled in all of the required information.");
            else
            {
                // if research student is selected
                if (cboStudentType.SelectedValue == cbi2)
                {
                    string levelCheck = "";

                    try
                    {
                        levelCheck = researchStudent.award();
                    }

                    catch (Exception excep)
                    {
                        MessageBox.Show(excep.Message);
                    }
                    if (levelCheck == "Doctor of Philosophy")
                    {
                        Window newWindow = new Award_GUI(researchStudent);
                        newWindow.ShowDialog();
                    }
                }
                else
                { 
                    // Creates a new Award_GUI window, also passing in as a constructor argument the current student object.
                    Window newWindow = new Award_GUI(student);
                    newWindow.ShowDialog();
                }
                
            }
        }

        // Clears prompt text from textboxes
        public void PromptClear(object sender)
        {
            TextBox tb = (TextBox)sender;
            tb.Foreground = Brushes.Black;
            tb.Text = string.Empty;
            //tb.GotFocus -= txtBoxSecondName_GotFocus;
        }

        // Disables and enables researchStudent fields depending on what the user has selected on cboStudentType combobox.
        private void cboStudentType_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            // If selection is Research Student
            if (cboStudentType.SelectedValue == cbi2)
            {
                txtBoxTopic.IsEnabled = true;
                lblTopic.IsEnabled = true;
                txtBoxSupervisor.IsEnabled = true;
                lblSupervisor.IsEnabled = true;

                txtBoxCourse.Text = researchStudent.Course;
                txtBoxCourse.IsEnabled = false;
            }
            // If selection is Student
            else
            {
                txtBoxTopic.IsEnabled = false;
                lblTopic.IsEnabled = false;
                txtBoxSupervisor.IsEnabled = false;
                lblSupervisor.IsEnabled = false;

                txtBoxCourse.IsEnabled = true;
                txtBoxCourse.Text = "eg. Computing";
            }
        }
    }
}
