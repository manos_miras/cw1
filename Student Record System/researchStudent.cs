﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/*  
    The purpose of the ResearchStudent class is to store and validate relevant ResearchStudent information.
    Made by Emmanuel Miras (40168970) as part of the SD2 course.
    Date last modified: 22/10/2015
*/

namespace Student_Record_System
{
    public class ResearchStudent : Student
    {
        #region properties

        // Stores the research student topic
        private string _topic;

        // Stores the research student supervisor
        private string _supervisor;

        #endregion properties

        #region accessors

        // Gets or sets the value of topic
        public string Topic
        {
            get { return _topic; }
            set
            {
                if (value == "" || value == "Topic title")
                {
                    throw new ArgumentException("Topic can not be blank.");
                }
                _topic = value;
            }
        }

        // Gets or sets the value of supervisor
        public string Supervisor
        {
            get { return _supervisor; }
            set
            {
                if (value == "" || value == "Name of supervisor")
                {
                    throw new ArgumentException("Supervisor can not be blank.");
                }
                _supervisor = value;
            }
        }

        #endregion accessors

        #region constructors

        public ResearchStudent()
        {
            Course = "PhD";
        }

        public ResearchStudent(string firstName, string secondName, string dateOfBirth,
                     string matriculationNumber, string level, string credits, string topic, string supervisor, ref bool validated)
        {
            try
            {
                FirstName = firstName;                                      // Assign student first name
                SecondName = secondName;                                    // Assign student second name

                DateTime validDateTime;                                     // Date of birth field validation.
                if (!DateTime.TryParse(dateOfBirth, out validDateTime))      // If the date of birth field has invalid characters, an error is shown.
                    throw new ArgumentException("Please enter a valid date into the Date of Birth text box");
                DateOfBirth = validDateTime;

                Course = "PhD";

                if (isNumeric(matriculationNumber, "Please enter a number between 40000 and 60000 into the Matriculation Number text box"))
                    MatriculationNumber = ushort.Parse(matriculationNumber);

                if (isNumeric(level, "Please enter a number between 1 and 4 into the Level text box"))
                    Level = ushort.Parse(level);
                if (isNumeric(credits, "Please enter a number between 0 and 480 into the Credits text box"))
                    Credits = ushort.Parse(credits);

                Topic = topic;                     // Assign research student topic
                Supervisor = supervisor;           // Assign research student supervisor

                validated = true;
            }
            catch (Exception excep)
            {
                validated = false;
                MessageBox.Show(excep.Message);
            }
        }

        #endregion constructors

        #region methods
        public override string award()
        {
            if (Level == 4)
            {
                return "Doctor of Philosophy";
            }
            else
                throw new ArgumentException("A research student may only be awarded if he has reached level 4.");
        }

        #endregion methods

    }
}
