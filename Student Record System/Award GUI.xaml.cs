﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

/*  
    The purpose of the Award_GUI Window is to display Student and ResearchStudent awards.
    Made by Emmanuel Miras (40168970) as part of the SD2 course.
    Date last modified: 22/10/2015
*/

namespace Student_Record_System
{
    public partial class Award_GUI : Window
    {
        // Student is required to be entered as a constructor arguments when Award_GUI is called.
        public Award_GUI(Student student)
        {
            InitializeComponent();

            // Assigns student values to relevant labels.
            lblAwardMatric.Content = student.MatriculationNumber;
            lblAwardSecondName.Content = student.SecondName;
            lblAwardFirstName.Content = student.FirstName;
            lblAwardCourse.Content = student.Course;
            lblAwardLevel.Content = student.Level;
            lblAwardAward.Content = student.award();
        }

        public Award_GUI(ResearchStudent researchStudent)
        {
            InitializeComponent();

            // Assigns researchStudent values to relevant labels.
            lblAwardMatric.Content = researchStudent.MatriculationNumber;
            lblAwardSecondName.Content = researchStudent.SecondName;
            lblAwardFirstName.Content = researchStudent.FirstName;
            lblAwardCourse.Content = researchStudent.Course;
            lblAwardLevel.Content = researchStudent.Level;
            lblAwardAward.Content = researchStudent.award();

        }
        
        // On close button click
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            // Closes the Award_GUI window.
            this.Close();
        }
    }
}
