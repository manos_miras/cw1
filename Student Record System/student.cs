﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/*  
    The purpose of the Student class is to store and validate relevant student information.
    Made by Emmanuel Miras (40168970) as part of the SD2 course. 
    Date last modified: 21/10/2015 
*/


namespace Student_Record_System
{
    public class Student
    {
        #region properties

        // Stores matriculation number of student (In the range 40000 to 60000)
        private ushort _matriculationNumber;

        // Stores first name of the student
        private string _firstName;

        // Stores second name of the student
        private string _secondName;

        // Stores the date of birth of student
        private DateTime _dateOfBirth;

        // Stores the course of the student
        private string _course;

        // Stores the level of the student (In the range 1-4)
        private ushort _level;

        // Stores student credits (In the range 0 to 480)
        private ushort _credits;

        #endregion

        #region accessors

        // Gets or sets the value of matriculationNumber
        public ushort MatriculationNumber 
        {
            get { return _matriculationNumber; }
            set
            {
                // If the value entered is not between the range of 40000 to 60000, an error is shown
                if (value < 40000 || value > 60000)
                {
                    throw new ArgumentException("Invalid matriculation number.");
                }
                _matriculationNumber = value;

            }
        }

        // Gets or sets the value of firstName
        public string FirstName
        {
            get { return _firstName; }
            
            set
            {
                // If the value entered is blank, an error is shown
                if (value == "" || value == "First name")
                {
                    throw new ArgumentException("First name can not be blank.");
                }
                _firstName = value;
            }
        }

        // Gets or sets the value of secondName
        public string SecondName
        {
            get { return _secondName; }
            set
            {
                // If the value entered is blank, an error is shown
                if (value == "" || value == "Second name")
                {
                    throw new ArgumentException("Second name can not be blank.");
                }
                _secondName = value;
            }
        }

        // Gets or sets the value of dateOfBirth
        public DateTime DateOfBirth
        {
            get { return _dateOfBirth; }
            set
            {
                // If the value entered is blank, an error is shown
                if (value == null)
                {
                    throw new ArgumentException("Date of birth can not be blank.");
                }
                _dateOfBirth = value;
            }
        }

        // Gets or sets the value of course
        public string Course
        {
            get { return _course; }
            set
            {
                // If the value entered is blank, an error is shown
                if (value == "" || value == "eg. Computing")
                {
                    throw new ArgumentException("Course can not be blank.");
                }
                _course = value;
            }
        }

        // Gets or sets the value of level
        public ushort Level
        {
            get { return _level; }
            set
            {
                if (value < 1 || value > 4)
                {
                    throw new ArgumentException("Invalid student level.");
                }
                _level = value;
            }
        }

        // Gets or sets the value of credits
        public ushort Credits
        {
            get { return _credits; }
            set
            {
                if (value < 0 || value > 480)
                {
                    throw new ArgumentException("Invalid amount of credits.");
                }
                _credits = value;
            }
        }

        #endregion

        #region constructors

        public Student()
        {

        }

        public Student(string firstName, string secondName, string dateOfBirth, string course,
               string matriculationNumber, string level, string credits, ref bool validated)
        {
            try
            {
                FirstName = firstName;                                     // Assign student first name
                SecondName = secondName;                                   // Assign student second name

                DateTime validDateTime;                                     // Date of birth field validation.
                if (!DateTime.TryParse(dateOfBirth, out validDateTime))      // If the date of birth field has invalid characters, an error is shown.
                    throw new ArgumentException("Please enter a valid date into the Date of Birth text box");
                DateOfBirth = validDateTime;

                Course = course;

                if (isNumeric(matriculationNumber, "Please enter a number between 40000 and 60000 into the Matriculation Number text box"))
                    MatriculationNumber = ushort.Parse(matriculationNumber);

                if (isNumeric(level, "Please enter a number between 1 and 4 into the Level text box"))
                    Level = ushort.Parse(level);
                if (isNumeric(credits, "Please enter a number between 0 and 480 into the Credits text box"))
                    Credits = ushort.Parse(credits);

                validated = true;
            }
            catch (Exception excep)
            {
                validated = false;
                MessageBox.Show(excep.Message);
            }
        }

        #endregion

        #region methods
        // Advances the student by 1 level
        public void advance()
        {
            switch (_level)
            {
                case 1:
                    if (_credits >= 120)
                        _level = 2;
                    else
                        MessageBox.Show("Insufficient credits, student may not advance.");
                    break;
                case 2:
                    if (_credits >= 240)
                        _level = 3;
                    else
                        MessageBox.Show("Insufficient credits, student may not advance.");
                    break;
                case 3:
                    if (_credits >= 360)
                        _level = 4;
                    else
                        MessageBox.Show("Insufficient credits, student may not advance.");
                    break;
                case 4:
                    MessageBox.Show("Student is already in 4th year");
                    break;
            }
        }

        // Will return a message displaying the award to be conferred to a student.
        public virtual string award()
        {
            if (_credits >= 0 && _credits <= 359)
                return ("Certificate of Higher Education");
            else if (_credits >= 360 && _credits <= 479)
                return ("Degree");
            else
                return ("Honours degree");

        }

        //Checks if a value is numeric or not
        public static bool isNumeric(string input, string ExceptionString)
        {
            //Int output: Used to hold in the output from TryParse
            ushort output;
            //Checks if entered value is numeric or not
            if (!ushort.TryParse(input, out output))

                throw new ArgumentException(ExceptionString);
            else
                return true;
        }


        #endregion

    }
}
